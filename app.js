"use strict";

const tabsTitle = document.querySelectorAll(".tabs-title");
const tabsContent = document.querySelectorAll(".tabs-content__item");

const arrayOfTabs = [...tabsTitle];
const arrayOfTabsContent = [...tabsContent];

function removeTabsContent(array) {
  array.forEach((element) => {
    element.style.display = "none";
  });
}

function removeClassActive(array) {
  array.forEach((element) => {
    element.classList.remove("active");
  });
}

arrayOfTabs.forEach((element) => {
  element.addEventListener("click", () => {
    removeClassActive(arrayOfTabs);
    element.classList.add("active");
    arrayOfTabsContent.forEach((item) => {
      if (
        element.innerText.toLowerCase() === item.dataset.title.toLowerCase()
      ) {
        removeTabsContent(arrayOfTabsContent);
        item.style.display = "block";
      }
    });
  });
});
